package ru.tsc.avramenko.tm.exception.entity;

import ru.tsc.avramenko.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found.");
    }

}